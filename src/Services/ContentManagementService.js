/* global Firebase */

class ContentManagementService {
  static initializeContent(dataSnapshot) {
    if (!ContentManagementService.content) {
      ContentManagementService.content = dataSnapshot.val();
      ContentManagementService.content.parsed = {}
      ContentManagementService.content.parsed.menuNames = {}

      for (var i = 0; i < ContentManagementService.content.length; i++) {
        ContentManagementService.content.parsed[ContentManagementService.content[i].page] = ContentManagementService.content[i].pageSpecificData[0];
        ContentManagementService.content.parsed.menuNames[ContentManagementService.content[i].page] = ContentManagementService.content[i].pageSpecificData[0].menuName;
      }
    }

    return ContentManagementService.content.parsed;
  }
}

ContentManagementService.content = null;
ContentManagementService.firebase = new Firebase("https://birddogapplicati-1545605214193.firebaseio.com/birddogcms");
ContentManagementService.aboutBirddog = null;
ContentManagementService.applyNow = null;
ContentManagementService.careers = null;
ContentManagementService.careerSense = null;
ContentManagementService.home = null;


export default ContentManagementService
