/* global gapi */

import { Base64 } from 'js-base64';

class EmailService {

  static handleClientLoad() {
    return gapi.load('client:auth2', EmailService.initClient);
  }

  static initClient() {
    gapi.client.init({
      clientId: EmailService.AUTH.client_id,
      discoveryDocs: EmailService.DISCOVERY_DOC,
      scope: EmailService.SCOPES
    }).then(function () {
      // Handle the initial sign-in state.
      if (!gapi.auth2.getAuthInstance().isSignedIn.get()) {
        gapi.auth2.getAuthInstance().signIn();
      }
    }, function(error) {
      console.log(JSON.stringify(error, null, 2));
    });
  }

  static sendEmail(formInfo, attachment) {

    var draft = [
      'From: me\r\n',
      `To: ${process.env.REACT_APP_EMAIL_DESTINATION}\r\n`,
      `Subject: New Birddog Application Submitted by ${formInfo.firstName}!\r\n`,
      'MIME-Version: 1.0\r\n',
      'Content-Type: multipart/mixed; boundary="MixedBoundaryString"\r\n\r\n',

      '--MixedBoundaryString\r\n',
      'Content-Type: multipart/alternative; boundary="AlternativeBoundaryString"\r\n\r\n',

      '--AlternativeBoundaryString\r\n',
      'Content-Type: text/plain;charset="utf-8"\r\n',
      'Content-Transfer-Encoding: quoted-printable\r\n\r\n',

      'Hey Blake!\r\n\r\n',

      `${formInfo.firstName} in ${formInfo.state} has just submitted an application. Check it out!\r\n`,

      '________________________________________________\r\n',
      `Name: ${formInfo.firstName} ${formInfo.lastName}\r\n`,
      `Phone: ${formInfo.phoneNumber}\r\n`,
      `Email: ${formInfo.emailAddress}\r\n`,
      `Address: ${formInfo.address1} ${formInfo.address2}, ${formInfo.city}, ${formInfo.state} ${formInfo.zip}\r\n`,
      `Career: ${formInfo.career}\r\n`,
      `Career Location: ${formInfo.careerLocation}\r\n\r\n`,
      '________________________________________________\r\n',

      `Background: ${formInfo.background}\r\n\r\n`,

      '________________________________________________\r\n',
      `Career Goals: ${formInfo.careerGoal}\r\n\r\n`,
      '________________________________________________\r\n\r\n',
      '--AlternativeBoundaryString--\r\n\r\n',
    ];

    if (attachment) {
      const reader = new FileReader();

      draft.push(...[
        '--MixedBoundaryString\r\n',
        `Content-Type: ${attachment.type}; name="${attachment.name}"\r\n`,
        'Content-Transfer-Encoding: base64\r\n',
        `Content-Disposition: attachment;filename="${attachment.name}"\r\n\r\n`,
      ]);

      reader.onload = function (event) {
        draft.push([
          event.target.result.split(',')[1],
          '\r\n\r\n',
          '--MixedBoundaryString--',
        ]);
        const encodedDraft = Base64.encodeURI(draft.join(''));
        EmailService.sendMessage(encodedDraft)
      }

      reader.readAsDataURL(attachment);

    } else {
      draft.push('--MixedBoundaryString--');
      const encodedDraft = Base64.encodeURI(draft.join(''));
      EmailService.sendMessage(encodedDraft);
    }
  }

  static sendMessage(encodedMessage) {
    gapi.client.gmail.users.messages.send({
      'userId': 'me',
      'resource': {
        'raw': encodedMessage
      }
    })
    .then((res) => {
      console.log('EmailSent');
      console.log(res);
    })
    .catch((err) => {
      console.log('Error');
      console.log(err)
    })
  }
}

EmailService.AUTH = {
    client_id: "434474989383-qsk8r9jdfka8j7lenrq3ee60d4b8k3lg.apps.googleusercontent.com"
};
EmailService.DISCOVERY_DOC = ['https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest'];
EmailService.SCOPES = 'https://www.googleapis.com/auth/gmail.send'

export default EmailService
