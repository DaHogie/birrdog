
import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import AboutBirddog from 'Components/Pages/AboutBirddog/AboutBirddog';
import ApplyNow from 'Components/Pages/ApplyNow/ApplyNow';
import Careers from 'Components/Pages/Careers/Careers';
import CareerSense from 'Components/Pages/CareerSense/CareerSense'
import Footer from 'Components/Shared/Footer/Footer';
import HomePage from 'Components/Pages/HomePage/HomePage';
import Header from 'Components/Shared/Header/Header';
import Loading from 'Components/Shared/Loading/Loading';

import ContentManagementService from 'Services/ContentManagementService';

class RouterEntry extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoadingData: true,
      content: {}
    };
  }

  componentDidMount() {
    ContentManagementService.firebase.on('value', (dataSnapshot) => {
      this.setState({
        isLoadingData: false,
        content: ContentManagementService.initializeContent(dataSnapshot)
      });

      console.log(this.state);
    });
    setTimeout(() => {
      this.setState({
        isLoadingData: false,
      });
    }, 3000);
  }

  componentWillUnmount() {
    ContentManagementService.firebase.off();
  }

  render() {
    let page;
    if (this.state.isLoadingData) {
      page = (
        <div>
          <Loading/>
          <Footer></Footer>
        </div>
      );
    } else {
      page = (
        <div>
          <Header menuNames={this.state.content.menuNames}></Header>
          <Route exact path="/" render={ routeProps => <HomePage {...routeProps} homeContent={this.state.content.home}/> } />
          <Route path="/applynow" render={ routeProps => <ApplyNow {...routeProps} applyNowContent={this.state.content.applyNow}/> } />
          <Route exact path="/careers" render={ routeProps => <Careers {...routeProps} careersContent={this.state.content.careers}/> } />
          <Route path="/careersense" render={ routeProps => <CareerSense {...routeProps} careerSenseContent={this.state.content.careerSense}/> } />
          <Route path="/aboutbirddog" render={ routeProps => <AboutBirddog {...routeProps} aboutBirddogContent={this.state.content.aboutBirddog}/> } />
          <Footer></Footer>
        </div>);
    }
    return (
      <Router>
        <div>
          {page}
        </div>
      </Router>
    );
  }
}

export default RouterEntry
