import React, { Component } from 'react';
import {
  Button,
  Col,
  Row,
  Form,
  FormGroup,
  Label,
  Input,
  FormText } from 'reactstrap';

import EmailService from 'Services/EmailService';
import 'Components/Shared/Application/Application.css';

class Application extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address1: '',
      address2: '',
      background: '',
      career: 'Painter',
      careerGoal: '',
      careerLocation: 'Colorado',
      city: '',
      emailAddress: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      state: 'AK',
      isValidForm: false,
      zip: '',
    };

    this.careers = [
      'Painter',
      'Welder',
      'Transportation',
      'Pilot',
    ];

    this.careerLocations = [
      'Tennessee',
      'Ohio',
      'Colorado',
      'Chicago',
    ];

    this.states = [
      'AK',
      'AL',
      'AZ',
      'AR',
      'CA',
      'CO',
      'CT',
      'DE',
      'FL',
      'GA',
      'HI',
      'ID',
      'IL',
      'IN',
      'IA',
      'KS',
      'KY',
      'LA',
      'ME',
      'MD',
      'MA',
      'MI',
      'MN',
      'MS',
      'MO',
      'MT',
      'NE',
      'NV',
      'NH',
      'NJ',
      'NM',
      'NY',
      'NC',
      'ND',
      'OH',
      'OK',
      'OR',
      'PA',
      'RI',
      'SC',
      'SD',
      'TN',
      'TX',
      'UT',
      'VT',
      'VA',
      'WA',
      'WV',
      'WI',
      'WY',
    ];

    this.fileUpload = React.createRef();

    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleFormSubmission = this.handleFormSubmission.bind(this);

  }

  handleFormChange(event) {
    const target = event.target;
    const name = target.name;

    this.setState({
      [name]: target.value,
    });
  }

  handleFormSubmission(event) {
    event.preventDefault();
    EmailService.sendEmail(this.state, this.fileUpload.current.files[0]);
  }

  render() {
    return (
      <Form className="Application" onSubmit={this.handleFormSubmission}>
        <Row form>
          <Col>
            <FormGroup>
              <Label for="firstName">First Name *</Label>
              <Input
                required
                id="firstName"
                name="firstName"
                type="text"
                value={this.state.firstName}
                onChange={this.handleFormChange}
                placeholder="First Name"
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="lastName">Last Name *</Label>
              <Input
                required
                id="lastName"
                name="lastName"
                type="text"
                value={this.state.lastName}
                onChange={this.handleFormChange}
                placeholder="Last Name"
              />
            </FormGroup>
          </Col>
        </Row>
        <Row form>
          <Col>
            <FormGroup>
              <Label for="emailAddress">Email *</Label>
              <Input
                required
                id="emailAddress"
                name="emailAddress"
                type="email"
                value={this.state.emailAddress}
                onChange={this.handleFormChange}
                placeholder="Email Address"
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="phoneNumber">Phone Number *</Label>
              <Input
                required
                id="phoneNumber"
                name="phoneNumber"
                type="tel"
                value={this.state.phoneNumber}
                onChange={this.handleFormChange}
                placeholder="Phone Number"
              />
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="address1">Address *</Label>
          <Input
            required
            id="address1"
            name="address1"
            type="text"
            value={this.state.address1}
            onChange={this.handleFormChange}
            placeholder="1234 Main Street"
          />
        </FormGroup>
        <FormGroup>
          <Label for="address2">Address 2</Label>
          <Input
            id="address2"
            name="address2"
            type="text"
            value={this.state.address2}
            onChange={this.handleFormChange}
            placeholder="Apartment, Suite, or Floor"
          />
        </FormGroup>
        <Row form>
          <Col md={7}>
            <FormGroup>
              <Label for="city">City *</Label>
              <Input
                required
                id="city"
                name="city"
                type="text"
                value={this.state.city}
                onChange={this.handleFormChange}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="state">State *</Label>
              <Input
                required
                id="state"
                name="state"
                type="select"
                value={this.state.state}
                onChange={this.handleFormChange}
              >
              {this.states.map((state, index) =>
                <option key={index} value={state}>{state}</option>
              )}
              </Input>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="zip">Zip Code *</Label>
              <Input
                required
                id="zip"
                name="zip"
                type="text"
                value={this.state.zip}
                onChange={this.handleFormChange}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row form>
          <Col>
            <FormGroup>
              <Label for="career">Career *</Label>
              <Input
                required
                id="career"
                name="career"
                type="select"
                value={this.state.career}
                onChange={this.handleFormChange}
              >
              {this.careers.map((career, index) =>
                <option key={index} value={career}>{career}</option>
              )}
              </Input>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="careerLocation">Career Location *</Label>
              <Input
                required
                id="careerLocation"
                name="careerLocation"
                type="select"
                value={this.state.careerLocation}
                onChange={this.handleFormChange}
              >
                {this.careerLocations.map((location, index) =>
                  <option key={index} value={location}>{location}</option>
                )}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="background">Background</Label>
          <Input
            id="background"
            name="background"
            type="textarea"
            value={this.state.background}
            onChange={this.handleFormChange}
            placeholder="Tell us about yourself!"
          />
        </FormGroup>
        <FormGroup>
          <Label for="careerGoal">Career Goal</Label>
          <Input
            id="careerGoal"
            name="careerGoal"
            type="textarea"
            value={this.state.careerGoal}
            onChange={this.handleFormChange}
            placeholder="What are you hoping to achieve?"
          />
        </FormGroup>
        <Label for="resume">Resume</Label>
        <FormGroup>
          <input
            id="resume"
            name="resume"
            type="file"
            ref={this.fileUpload}
          />
          <FormText color="muted">
            If you have a resume, please upload it here in addition to filling out the form fields above
          </FormText>
        </FormGroup>
        <div className="text-center">
          <Button>Submit</Button>
        </div>
      </Form>
    );
  }
}

export default Application
