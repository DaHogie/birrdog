import React from 'react';
import ReactDOM from 'react-dom';
import Application from 'Components/Shared/Application/Application';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Application />, div);
  ReactDOM.unmountComponentAtNode(div);
});
