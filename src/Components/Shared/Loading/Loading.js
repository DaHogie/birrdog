import React, { Component } from 'react';
import 'Components/Shared/Loading/Loading.css';

class Loading extends Component {

  render() {
    return (
      <div className="Loading">
        Loading...
      </div>
    );
  }
}

export default Loading;
