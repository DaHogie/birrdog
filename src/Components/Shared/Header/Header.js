import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink, } from 'reactstrap'
import { Link } from 'react-router-dom'
import 'Components/Shared/Header/Header.css';

class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <div className="Header">
        <Navbar color="dark" dark expand="md">
          <NavbarBrand tag={Link} exact="true" to="/">{this.props.menuNames.home}</NavbarBrand>
          <NavbarToggler onClick={this.toggle}/>
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={Link} to="/applynow">{this.props.menuNames.applyNow}</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/careers">{this.props.menuNames.careers}</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/careersense">{this.props.menuNames.careerSense}</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/aboutbirddog">{this.props.menuNames.aboutBirddog}</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

Header.defaultProps = {
  menuNames: {}
};

export default Header;
