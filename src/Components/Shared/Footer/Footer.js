import React, { Component } from 'react';
import 'Components/Shared/Footer/Footer.css';

class Footer extends Component {

  render() {
    return (
      <div className="Footer">
        Copyright &copy; 2018 Birddog Workforce Scouting.
      </div>
    );
  }
}

export default Footer;
