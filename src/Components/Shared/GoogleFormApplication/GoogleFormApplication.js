import React, { Component } from 'react';

import 'Components/Shared/GoogleFormApplication/GoogleFormApplication.css';

class GoogleFormApplication extends Component {

  render() {
    return (
      <div className="gformcontainer embed-responsive embed-responsive-4by3">
        <iframe className="embed-responsive-item" title="BirddogApplication" src="https://docs.google.com/forms/d/e/1FAIpQLSeAMvPAFPntTc3Mpaoj0br5xxc4OnljWz9Ain__gx3uxF3qrw/viewform?embedded=true" width="640" height="600" frameBorder="0">Loading...</iframe>
      </div>
    );
  }
}

export default GoogleFormApplication
