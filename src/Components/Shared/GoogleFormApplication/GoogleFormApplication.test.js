import React from 'react';
import ReactDOM from 'react-dom';
import GoogleFormApplication from 'Components/Shared/GoogleFormApplication/GoogleFormApplication';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GoogleFormApplication />, div);
  ReactDOM.unmountComponentAtNode(div);
});
