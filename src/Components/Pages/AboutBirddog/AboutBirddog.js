import React, { Component } from 'react';

import 'index.css';
import 'Components/Pages/AboutBirddog/AboutBirddog.css';

class AboutBirddog extends Component {
  render() {
    return (
      <div className="newPage AboutBirddog">
        <p className="birddogVideo__Header">{this.props.aboutBirddogContent.aboutBirddogHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="AboutBirddog" width="560" height="315" src="https://www.youtube.com/embed/9snXa935t70" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText"></p>
      </div>
    );
  }
}

AboutBirddog.defaultProps = {
  aboutBirddogContent: {
    aboutBirddogHeader: 'Watch the video below to learn what Birddog is all about!'
  }
}

export default AboutBirddog;
