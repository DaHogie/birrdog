import React from 'react';
import ReactDOM from 'react-dom';
import AboutBirddog from 'Components/Pages/AboutBirddog/AboutBirddog';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AboutBirddog />, div);
  ReactDOM.unmountComponentAtNode(div);
});
