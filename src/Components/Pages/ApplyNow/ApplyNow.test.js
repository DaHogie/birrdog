import React from 'react';
import ReactDOM from 'react-dom';
import ApplyNow from 'Components/Pages/ApplyNow/ApplyNow';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ApplyNow />, div);
  ReactDOM.unmountComponentAtNode(div);
});
