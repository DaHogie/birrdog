import React, { Component } from 'react';

import GoogleFormApplication from 'Components/Shared/GoogleFormApplication/GoogleFormApplication';

import 'index.css';
import 'Components/Pages/ApplyNow/ApplyNow.css';

class ApplyNow extends Component {
  render() {
    return (
      <div className="newPage">
        <GoogleFormApplication></GoogleFormApplication>
      </div>
    );
  }
}

export default ApplyNow;
