import React, { Component } from 'react';

import logo from 'images/birddogLogo.png';

import 'index.css';
import 'Components/Pages/HomePage/HomePage.css';

class HomePage extends Component {
  render() {
    return (
      <div className="newPage">
        <header>
          <img src={logo} className="Homepage__logo" alt="logo" />
          <p className="descriptionText">{this.props.homeContent.welcomeMessage}</p>
        </header>
      </div>
    );
  }
}

HomePage.defaultProps = {
  homeContent: {
    welcomeMessage: 'Welcome to Birddog Workforce Scouting!'
  }
};

export default HomePage;
