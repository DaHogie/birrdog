import React, { Component } from 'react';

import 'index.css';
import 'Components/Pages/Careers/Careers.css';

class Careers extends Component {
  render() {
    return (
      <div className="newPage">
        <p className="birddogVideo__Header">{this.props.careersContent.entryLevelAirplanePainterHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="EntryLevelAirplanePainter" width="560" height="315" src="https://www.youtube.com/embed/foZDHThJ1tY" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText">{this.props.careersContent.entryLevelAirplanePainterDescription}</p>
        <hr/>
        <p className="birddogVideo__Header">{this.props.careersContent.propaneSpecialistCareerOpportunitiesHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="PropaneSpecialistCareerOpportunities" width="560" height="315" src="https://www.youtube.com/embed/UjX756qDKoY" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText">{this.props.careersContent.propaneSpecialistCareerOpportunitiesDescription}</p>
      </div>
    );
  }
}

Careers.defaultProps = {
  careersContent: {
    entryLevelAirplanePainterHeader: 'Entry Level Airplane Painter',
    entryLevelAirplanePainterDescription: 'This job is available in Chattanooga, Tennessee; Grand Junction, Colorado; and East Alton, Illinois! Head on over to "Apply Now!" and get your application in if you\'re interested in being considered for the position!'
  }
};

export default Careers;
