import React from 'react';
import ReactDOM from 'react-dom';
import CareerSense from 'Components/Pages/CareerSense/CareerSense';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CareerSense />, div);
  ReactDOM.unmountComponentAtNode(div);
});
