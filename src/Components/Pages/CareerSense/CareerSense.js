import React, { Component } from 'react';


import 'index.css';
import 'Components/Pages/CareerSense/CareerSense.css';


class CareerSense extends Component {
  render() {
    return (
      <div className="newPage">
        <p className="birddogVideo__Header">{this.props.careerSenseContent.careerSenseHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="CareerSenseDescription" width="560" height="315" src="https://www.youtube.com/embed/hOjH_IGwvPA" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText">{this.props.careerSenseContent.careerSenseDescription}</p>
        <hr/>
        <p className="birddogVideo__Header">{this.props.careerSenseContent.paintIndustrialCoatingsHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="PaintAndIndustrialCoating" width="560" height="315" src="https://www.youtube.com/embed/X9-xdb3hyVo" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText">{this.props.careerSenseContent.paintIndustrialCoatingsDescription}</p>
        <hr/>
        <p className="birddogVideo__Header">{this.props.careerSenseContent.autoCollisionSchoolsAndScholarshipsHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="AutoCollisionSchoolsAndScholarships" width="560" height="315" src="https://www.youtube.com/embed/_BDSc0HnwB4" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText">{this.props.careerSenseContent.autoCollisionSchoolsAndScholarshipsDescription}</p>
        <hr/>
        <p className="birddogVideo__Header">{this.props.careerSenseContent.birddogToolTruckDASanderHeader}</p>
        <div className="birddogVideo embed-responsive embed-responsive-4by3">
          <iframe className="embed-responsive-item" title="BirddogToolTruckDASander" width="560" height="315" src="https://www.youtube.com/embed/5IsXPN_vUSU" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <p className="descriptionText">{this.props.careerSenseContent.birddogToolTruckDASanderDescription}</p>
      </div>
    );
  }
}

CareerSense.defaultProps = {
  careerSenseContent: {
    careerSenseHeader: 'Career Sense Explanation',
    careerSenseDescription: 'This video gives you the details on what the career sense video series will be like for each industry. Watch to learn more!',
    paintIndustrialCoatingsHeader: 'First Video of Paint and Industrial Coating Careers Series',
    paintIndustrialCoatingsDescription: 'Checkout the first video of Blake\'s Career Sense series on the Paint and Industrial Coatings Industry!'
  }
}

export default CareerSense;
