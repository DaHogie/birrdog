import React from 'react';
import ReactDOM from 'react-dom';
import RouterEntry from 'Components/RouterEntry';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RouterEntry />, div);
  ReactDOM.unmountComponentAtNode(div);
});
